#!/usr/bin/env python3

# WebServiceWatcher
# Copyright (C) 2019  Sohan Muthukumara
# Initial Release - 2018/06/27
# This script will periodically monitor all URLs configured in config.ini and
# send an email notification if a request does not return 200 or if the response is empty.
# This script can be executed with the command line option '--debug' to enable debug logs.

import requests
import configparser
import time
import threading
import smtplib
import sys
import os
import logging
from email.message import EmailMessage


smtp_server = 'mail.myserver.com'
from_address = 'webservicewatcher@mailserver.com'


def send_email(message):
    try:
        with smtplib.SMTP(smtp_server, timeout=10) as smtp_conn:
            smtp_conn.send_message(message)
        logger.info("Email notification sent")
    except:
        logger.error("Failed to send email notification. SMTP Connection Error")


def send_down_notification(name, url, to_address, returned_status, content_length):
    msg = EmailMessage()
    msg['Subject'] = "[CRITICAL] - {} is Down".format(name)
    msg['From'] = from_address
    msg['To'] = to_address
    msg.set_content("""\
    Returned status: {} \n
    Response content length: {} \n
    {}
    """.format(returned_status, content_length, url))
    logger.info("Sending notification to: {}".format(to_address))
    send_email(msg)


def send_up_notification(name, url, to_address, returned_status, content_length):
    msg = EmailMessage()
    msg['Subject'] = "[RESOLVED] - {} is Up".format(name)
    msg['From'] = from_address
    msg['To'] = to_address
    msg.set_content("""\
    Returned status: {} \n
    Response content length: {} \n
    {}
    """.format(returned_status, content_length, url))
    logger.info("Sending notification to: {}".format(to_address))
    send_email(msg)


def send_timeout_notification(name, url, timeout, to_address):
    msg = EmailMessage()
    msg['Subject'] = "[WARNING] - {} is Timed Out".format(name)
    msg['From'] = from_address
    msg['To'] = to_address
    msg.set_content("""\
    Connection to {} timed out. \n
    Connect timeout: {} seconds \n
    """.format(url, timeout))
    logger.info("Sending notification to: {}".format(to_address))
    send_email(msg)


def check_status_get(name, url, interval, timeout, to_address):
    site_status = True
    while True:
        try:
            logger.debug("Sending Request: {}".format(url))
            response = requests.get(url, timeout=timeout)
        except requests.Timeout as err:
            logger.info("Timeout: {}".format(err))
            send_timeout_notification(name, url, timeout, to_address)
            time.sleep(interval)
            continue
        except requests.ConnectionError as err:
            logger.error("Connection Error: {}".format(err))
            time.sleep(interval)
            continue

        if response.status_code != 200 or len(response.content) <= 0:
            logger.info("{} is down. Returned: {}, Content Length: {}".format(name, response.status_code, len(response.content)))
            if site_status is True:
                send_down_notification(name, url, to_address, response.status_code, len(response.content))
                site_status = False
        else:
            if site_status is False:
                logger.info("{} is up. Returned: {}, Content Length: {}".format(name, response.status_code, len(response.content)))
                send_up_notification(name, url, to_address, response.status_code, len(response.content))
                site_status = True
        time.sleep(interval)


if __name__ == '__main__':
    logger = logging.getLogger("wsw")
    if len(sys.argv) > 1 and sys.argv[1] == "--debug":
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    FORMATTER = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    file_handler = logging.FileHandler(filename="wsw.log")
    file_handler.setFormatter(FORMATTER)
    logger.addHandler(file_handler)

    logger.info("####################### Web Service Watcher Started #######################")
    config = configparser.ConfigParser()
    config.read("config.ini")

    sites = {}

    for section in config.sections():
        if config.getboolean(section, 'enabled', fallback=True):
            sites[section] = {}
            try:
                sites[section]['url'] = config.get(section, 'url')
            except configparser.NoOptionError as err:
                print(err)
                logger.error(err)
                sys.exit()
            try:
                sites[section]['interval'] = config.getfloat(section, 'interval')
            except configparser.NoOptionError as err:
                print(err)
                logger.error(err)
                sys.exit()
            try:
                sites[section]['timeout'] = config.getfloat(section, 'timeout')
            except configparser.NoOptionError as err:
                print(err)
                logger.error(err)
                sys.exit()
            try:
                sites[section]['to_address'] = config.get(section, 'to_address')
            except configparser.NoOptionError as err:
                print(err)
                logger.error(err)
                sys.exit()

    logger.debug("Read from config: {}".format(sites))

    threads = []

    if len(sites) > 0:
        for site in sites:
            url = sites[site]['url']
            interval = sites[site]['interval']
            timeout = sites[site]['timeout']
            to_address = sites[site]['to_address']
            t = threading.Thread(name=site, target=check_status_get, args=(site, url, interval, timeout, to_address,))
            threads.append(t)

        for thread in threads:
            thread.start()
    else:
        print("No enabled sections in {}".format(os.path.join(os.getcwd(), "config.ini")))
        logger.error("No enabled sections in {}".format(os.path.join(os.getcwd(), "config.ini")))
        sys.exit()
